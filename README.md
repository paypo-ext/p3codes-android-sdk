# PayPoSDK
​
PayPo SDK for Android 
​
## Installation
​
```bash
repositories {
    maven {
        url "https://repository.paypo.pl/repository/maven2/"
    }
}
```
​
```bash
implementation "pl.paypo.android_sdk:android_sdk:1.0.3"
```
​
## Usage
​
```kotlin
import pl.paypo.sdk.PayPoSdk
​
class MyFragment : Fragment() {
​
    /* Declare ActivityResultLauncher<Intent!> */
    private val result = registerForActivityResult(StartActivityForResult()) {
        if (it.resultCode == RESULT_OK) {
            onSuccess()
        }
    }
​
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        /* Instantiate PayPoSdk */
        val sdk = PayPoSdk(
            context = requireContext(),
            url = "URL for WebView to open"
        )
​
        button.setOnClickListener {
            /* Launch WebView */
            result.launch(sdk.intent)
        }
    }
​
    private fun onSuccess() {
        Timber.d("PayPoSDK Closed")
    }
}
```
